import random


class Stickers:
    def get_f(self):
        return random.choice([
            'CAADAgADsAADTptkAler0GVnHyzGFgQ',
            'CAADAgADrwADTptkAoftMsdli6fnFgQ',
            'CAADAgADsgADTptkAm1WnTBWvUfiFgQ',
            'CAADAgADtAADTptkAq_OQoYz8ctCFgQ',
            'CAADAgADuQADTptkAn3O94Pvp3dmFgQ',
            'CAADAgADKAEAAk6bZAKZ9J-Sevw3ZRYE',
            'CAADAgADKQEAAk6bZAJ_ILxRYPzc6xYE',
            'CAADAgADTAEAAk6bZAIj7Q_wSl-eZhYE',
            'CAADAgADWQEAAk6bZAIMOnpSqkvAjhYE',
            'CAADAgADVAEAAk6bZAKIX8hX1c_u3BYE'
        ])

    def get_fail(self):
        return random.choice([
            'CAADAgAD0wEAAqnbXBTa2EGdm022axYE',
            'CAADAgADSB8AAqKKlgHgLysbaAOpExYE',
            'CAADBQADtgMAAukKyAN_IGYzBDoeZxYE',
            'CAADBQADnAMAAukKyAPo8e_mkstdpRYE',
            'CAADBQADeAMAAukKyAPTgGOWDk9SZxYE',
            'CAADBQADeQMAAukKyAOR2oP4lfTVoBYE'
        ])

    def get_dimooon(self):
        return 'CAADAgAD5AEAAjP4txXR9zwAAalvRooWBA'

    def get_server_up(self):
        return 'CAADBQADpAMAAukKyAPmSRSHIQIO2RYE'
