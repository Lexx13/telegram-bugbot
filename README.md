git clone https://Lexx13@bitbucket.org/Lexx13/telegram-bugbot.git bugbot

mkvirtualenv -p /usr/bin/python3 bugbot

workon bugbot

pip install -r requirements.txt

mkdir logs

chmod 775 logs

rm /home/main/.virtualenvs/bugbot/bin/postactivate

cp .env/default.postactivate .env/postactivate

sed -i -e 's/\r$//' .env/postactivate

ln -s `pwd`/.env/postactivate /home/main/.virtualenvs/bugbot/bin/postactivate

rm /home/main/.virtualenvs/bugbot/bin/predeactivate

cp .env/default.predeactivate .env/predeactivate

sed -i -e 's/\r$//' .env/predeactivate

ln -s `pwd`/.env/predeactivate /home/main/.virtualenvs/bugbot/bin/predeactivate

mkdir /home/main/.virtualenvs/bugbot/run

> Не используем переменную $VIRTUAL_ENV, т.к. если создаётся >=2 виртуальное окружение, 
> конфиги будут подтягиваться оттуда, а не из нашего нового окружения


sudo nano /etc/supervisor/conf.d/bugbot.conf

> Добавляем:

```
[program:BugBot]
directory = /webapps/bugbot
command = bash run.sh
user = main
redirect_stderr = true
stderr_logfile = /webapps/bugbot/logs/supervisor.log
stdout_logfile = /webapps/bugbot/logs/supervisor.log
stopsignal = TERM
stopasgroup = true
autorestart = true
```

sudo supervisorctl reread

sudo supervisorctl update

sudo nano /etc/nginx/sites-available/p1sms.conf

> Добавляем:
```
server {
    listen 443;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/P1sms/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/P1sms/privkey.pem;

    access_log /webapps/logs/nginx-access.log;
    error_log /webapps/logs/nginx-error.log;

    server_name telegram.p1sms.ru;

    location ~ .*/\.git {
        deny all;
    }

    proxy_redirect off;

    proxy_set_header         X-Real-IP $remote_addr;
    proxy_set_header         X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header         Host $http_host;

    location / {
        proxy_pass http://127.0.0.1:5057;
    }
}
```
