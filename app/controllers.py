import logging
from threading import Thread
from os import environ
from flask import request, jsonify
from app.utils.command_processor import CommandProcessor


def route_handler(app):
    @app.route('/tg/<token>', methods=['POST'])
    def telegram_callback_handler(token):
        if token != '{id}:{key}'.format(id=environ.get('TELEGRAM_BOT_ID'),
                                        key=environ.get('TELEGRAM_API_KEY')):
            return jsonify({'message': 'Permission denied'}), 403
        try:
            data = request.get_json(force=True)
            logging.info(data)
            Thread(target=CommandProcessor().telegram_command, args=(data,)).start()
        except Exception as e:
            logging.exception(e)
        return '', 200

    @app.route('/jira/callback', methods=['POST'])
    def jira_callback_handler():
        try:
            data = request.get_json(force=True)
            logging.info(data)
            Thread(target=CommandProcessor().jira_webhook, args=(data,)).start()
        except Exception as e:
            logging.exception(e)
        return '', 200
