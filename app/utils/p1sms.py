from app.utils.database import DatabaseHelper


class P1sms:
    statuses = {
        'error': 'Ошибка',
        'sent': 'Отправлена',
        'read': 'Прочтена',
        'delivered': 'Доставлена',
        'not_delivered': 'Не доставлена',
        'created': 'Создана, готова к отправке',
        'in_process': 'Отправляется...',
        'planned': 'Запланирована',
        'low_balance': 'Нет денег у клиента',
        'low_partner_balance': 'Нет денег у партнера',
        'creating': 'Создаётся - присваивается агрегатор, отправитель...',
        'auto_moderation': 'На автоматической модерации',
        'moderation': 'На модерации',
        'rejected': 'Отклонена модерацией',
        'pre_moderation': 'На автоматической модерации',
        'agg_error': 'Агрегатор не принял смс',
    }

    channels = {
        'digit': 'Цифра',
        'char': 'Буква',
        'viber': 'Viber',
        'voice': 'Голос',
        'vk': 'VK',
    }

    def __init__(self):
        self.db = DatabaseHelper()

    def find_sms_by_id(self, sms_id):
        result = []
        sms_info = self.db.p1sms_query("""
                        SELECT 
                            sms.id as id,
                            phones.phone as phone, 
                            aggregators.name as aggregator, 
                            aggregators.id as aggregator_id, 
                            senders.name as sender, 
                            tariffsenders.name as tariff_sender, 
                            smsstatuses.name as status, 
                            channels.name as channel, 
                            sms.created_at + interval '5 hour' as created_at, 
                            sms.sent_at + interval '5 hour' as sent_at,
                            sms.error_description as error_description,
                            operators.name as operator,
                            sms.message as text,
                            sms.delivery_id as delivery_id,
                            smssources.name as source
                        FROM sms 
                        JOIN phones ON sms.phone_id = phones.id 
                        JOIN operators ON phones.operator_id = operators.id
                        LEFT JOIN aggregators ON sms.aggregator_id = aggregators.id 
                        LEFT JOIN senders ON sms.sender_id = senders.id 
                        LEFT JOIN senders tariffsenders ON sms.tariff_sender_id = tariffsenders.id 
                        JOIN smsstatuses ON sms.status_id = smsstatuses.id 
                        JOIN smssources ON sms.source_id = smssources.id 
                        JOIN channels ON sms.channel_id = channels.id 
                        WHERE sms.id = {id}
                        """.format(id=sms_id)).as_dict()
        if len(sms_info) == 0:
            result.append('Смс с таким идентификатором не найдена')
        else:
            result.append('')
            self._make_sms_info_list(result, sms_info[0])

        return result

    def find_sms_by_phone(self, phone, limit):
        result = []
        sms_info = self.db.p1sms_query("""
                        SELECT
                          sms.id as id,
                          phones.phone as phone,
                          aggregators.name as aggregator,
                          aggregators.id as aggregator_id, 
                          senders.name as sender,
                          tariffsenders.name as tariff_sender,
                          smsstatuses.name as status,
                          channels.name as channel,
                          sms.created_at + interval '5 hour' as created_at,
                          sms.sent_at + interval '5 hour' as sent_at,
                          sms.error_description as error_description,
                          operators.name as operator,
                          sms.message as text,
                          sms.delivery_id as delivery_id,
                          smssources.name as source
                        FROM sms
                          JOIN phones ON sms.phone_id = phones.id
                          JOIN operators ON phones.operator_id = operators.id
                          LEFT JOIN aggregators ON sms.aggregator_id = aggregators.id
                          LEFT JOIN senders ON sms.sender_id = senders.id
                          LEFT JOIN senders tariffsenders ON sms.tariff_sender_id = tariffsenders.id
                          JOIN smsstatuses ON sms.status_id = smsstatuses.id
                          JOIN smssources ON sms.source_id = smssources.id 
                          JOIN channels ON sms.channel_id = channels.id
                        WHERE phones.phone = '{phone}'
                        ORDER BY sms.id DESC
                        LIMIT {limit}
                        """.format(phone=phone, limit=limit)).as_dict()
        if len(sms_info) == 0:
            result.append('Смс не найдены')
        else:
            result.append('')
            for sms in sms_info:
                self._make_sms_info_list(result, sms, True)
                result.append('')
                result.append('==========================')
                result.append('')
        return result

    def _make_sms_info_list(self, result, sms, with_id=False):
        if with_id:
            result.append('ID: ' + str(sms['id']))
        result.append('Номер: ' + sms['phone'])
        result.append('Оператор: ' + sms['operator'])
        result.append('Источник: ' + sms['source'])
        if sms['source'] == 'delivery':
            result.append('Рассылка: ' + str(sms['delivery_id']))
        result.append('Аггрегатор: ' + ('Не назначен' if sms['aggregator'] is None else (
                sms['aggregator'] + '(' + str(sms['aggregator_id']) + ')')))
        result.append(
            'Отправитель(по которому отправлена): ' + ('Отправителя нет' if sms['sender'] is None else sms['sender']))
        result.append(
            'Отправитель(по которому тарифицирована): ' + ('Отправителя нет' if sms['tariff_sender'] is None else
                                                           sms['tariff_sender']))
        result.append(
            'Статус: ' + (sms['status'] if sms['status'] not in self.statuses else self.statuses[sms['status']]))
        if sms['error_description'] is not None and sms['error_description'] != '':
            result.append('Ошибка наша или агрегатора: ' + sms['error_description'])
        result.append(
            'Канал: ' + (sms['channel'] if sms['channel'] not in self.channels else self.channels[sms['channel']]))
        result.append('Время создания: ' + sms['created_at'].strftime('%d.%m.%Y %H:%M:%S'))
        result.append('Время отправки: ' + ('Не отправлена' if sms['sent_at'] is None else sms['sent_at'].strftime(
            '%d.%m.%Y %H:%M:%S')))
        result.append('Текст: ' + sms['text'])
