class Users:
    def get_jira_users(self):
        return {
            '@lexx_v': 'Lexx',
            '@rainbowdash593': 'rainbowdash593',
            '@seniyaeclair': 'Kosheleva.yuliya.mmf',
            '@nezzl': 'Nezzl'
        }

    def get_jira_user(self, telegram_login):
        if telegram_login in self.get_jira_users():
            return self.get_jira_users()[telegram_login]
        else:
            raise ValueError

    def get_jira_auth_users(self):
        return ['lexx_v', 'rainbowdash593', 'seniyaeclair', 'nezzl']

    def get_p1sms_auth_users(self):
        return ['lexx_v']
