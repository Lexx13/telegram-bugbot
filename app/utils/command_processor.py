import requests
import logging
from os import environ
from app.exceptions.exceptions import *
from app.utils.bot import Bot, Actions, JiraActions
from storage.stickers.stickers import Stickers


class CommandProcessor:
    @staticmethod
    def telegram_command(data):
        try:
            if 'message' in data and 'text' in data['message']:
                bot = Bot(environ.get('TELEGRAM_BOT_ID'), environ.get('TELEGRAM_API_KEY'))
                stickers = Stickers()
                message = data['message']['text']
                chat_id = data['message']['chat']['id']
                username = data['message']['from']['username']
                try:
                    actions = Actions(message, chat_id, username)
                    if actions.is_command_exists():
                        actions.dispatch()
                except requests.RequestException:
                    bot.send_sticker(chat_id, stickers.get_fail())
                    bot.send_message(
                        chat_id,
                        'JIRA не отвечает :('
                    )
                except JiraNotAuthenticatedException:
                    bot.send_sticker(chat_id, stickers.get_fail())
                    bot.send_message(
                        chat_id,
                        'Вы не авторизованы в JIRA :('
                    )
                except P1smsNotAuthenticatedException:
                    bot.send_sticker(chat_id, stickers.get_fail())
                    bot.send_message(
                        chat_id,
                        'У вас нет прав на эту комманду :('
                    )
                except ValueError:
                    bot.send_sticker(chat_id, stickers.get_fail())
                    bot.send_message(
                        chat_id,
                        'Не смогли вас понять :('
                    )
            else:
                logging.info('Receive incorrect data payload')
        except Exception as exc:
            logging.exception(exc)

    @staticmethod
    def jira_webhook(data):
        try:
            JiraActions().task_updated(data)
        except Exception as exc:
            logging.exception(exc)
