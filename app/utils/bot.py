import re
import logging
import requests
import time
from os import environ

from app.utils.database import DatabaseHelper
from app.utils.jira import Jira
from app.exceptions.exceptions import *
from app.utils.p1sms import P1sms
from app.utils.users import Users
from storage.bot_info import BotInfo
from storage.images.images import Images
from storage.audio.audio import Audio
from storage.stickers.stickers import Stickers


class Bot:
    def __init__(self, bot_id, api_key):
        self.bot_id = bot_id
        self.api_key = api_key
        self.url = 'https://api.telegram.org/bot{bot_id}:{api_key}'.format(bot_id=bot_id, api_key=api_key)

    def send_message(self, chat_id, text):
        resp = None
        try:
            nowtime = time.time()
            text = text.replace('#', '(символ решетки)')
            req = requests.get(self.url + '/sendMessage?chat_id={chat_id}&text={text}'.format(chat_id=chat_id,
                                                                                              text=text))
            logging.info('Telegram sendMessage request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_sticker(self, chat_id, sticker_id):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendSticker?chat_id={chat_id}&sticker={sticker}'.format(chat_id=chat_id,
                                                                                                    sticker=sticker_id))
            logging.info('Telegram sendSticker request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_image(self, chat_id, image_id):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendPhoto?chat_id={chat_id}&photo={photo}'.format(chat_id=chat_id,
                                                                                              photo=image_id))
            logging.info('Telegram sendPhoto request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp

    def send_audio(self, chat_id, audio):
        resp = None
        try:
            nowtime = time.time()
            req = requests.get(self.url + '/sendAudio?chat_id={chat_id}&audio={audio}'.format(chat_id=chat_id,
                                                                                              audio=audio))
            logging.info('Telegram sendAudio request time: {}'.format(time.time() - nowtime))
            resp = req.json()
        except Exception as e:
            logging.error('Failed to send message: {e}'.format(e=e))
        return resp


class Actions:
    def __init__(self, text, chat_id, username):
        self.username = username
        self.text = text
        self.chat_id = chat_id
        self.command = text.split(' ')[0].lower() if len(text.split(' ')) else None
        self.tagged_members = re.findall(r'\@\w+', self.text)
        self.bot = Bot(environ.get('TELEGRAM_BOT_ID'), environ.get('TELEGRAM_API_KEY'))
        self.stickers = Stickers()
        self.images = Images()
        self.audio = Audio()
        self.bot_info = BotInfo()
        self.users = Users()
        # self.db = DatabaseHelper()
        # self.auth_user = self.db.query('SELECT * FROM users WHERE telegram_login = {login}'.format(login=username))

    def is_command_exists(self):
        return True if self.command else False

    def _get_title(self):
        title = self.text.replace(self.command, '')
        title = re.sub(r'\@\w+', '', title)
        return title.strip()

    def get_search_string(self):
        search = self.text.replace(self.command, '')
        return search.strip()

    def _create_bug(self):
        logging.info(self._get_title())
        if len(self.tagged_members):
            issues = []
            jira = Jira()
            for member in self.tagged_members:
                issue = jira.create_issue(self.chat_id, self._get_title(), member)
                issues.append(issue)
            self.bot.send_message(self.chat_id, 'Создаю для {members} задачи: \n{issues}'.format(
                members=', '.join(self.tagged_members),
                issues='\n'.join(issues)))

    def _find_issue(self):
        jira = Jira()
        issues = jira.search_issues_by_description(self.get_search_string())
        if len(issues) > 0:
            self.bot.send_message(self.chat_id, "Найденные карточки: %s" % ("\n".join(issues)))
        else:
            self.bot.send_message(self.chat_id, "Не найдено карточек по такому запросу")

    def _press_f(self):
        self.bot.send_sticker(self.chat_id, self.stickers.get_f())

    def _server_up(self):
        self.bot.send_sticker(self.chat_id, self.stickers.get_server_up())

    def _dimooon(self, midlength):
        if midlength > 2:
            if midlength >= 7:
                self.bot.send_sticker(self.chat_id, self.stickers.get_dimooon())
                self.bot.send_audio(self.chat_id, self.audio.get_dimooon())
            else:
                self.bot.send_message(self.chat_id, '@mushin_dmitriy')
                self.bot.send_image(self.chat_id, self.images.get_dimooon())

    def _show_info(self):
        self.bot.send_message(self.chat_id, self.bot_info.get())

    def _find_sms(self):
        sms_id = int(re.sub(r'\D', '', self.get_search_string()))
        if sms_id > 2147483647 or sms_id < 1:
            raise ValueError
        sms_info = P1sms().find_sms_by_id(sms_id)
        self.bot.send_message(self.chat_id, "Информация о смс {id}: {info}".format(
            id=sms_id,
            info="\n".join(sms_info)
        ))

    def _find_sms_by_phone(self):
        search_info = self.get_search_string().split(' ')

        phone = re.sub(r'\D', '', search_info[0])
        if len(phone) != 11:
            raise ValueError

        limit = 4
        if len(search_info) > 1:
            limit = re.sub(r'\D', '', search_info[1])
            if 0 < int(limit) <= 20:
                limit = int(limit)

        sms_info = P1sms().find_sms_by_phone(phone, limit)
        self.bot.send_message(self.chat_id, "Последние созданные смс на номер {phone}: {info}".format(
            phone=phone,
            info="\n".join(sms_info)
        ))

    def _save_jira_credentials(self):
        pass

    def _set_jira_project(self):
        pass

    def _is_authenticated_in_jira(self):
        if self.username not in self.users.get_jira_auth_users():
            raise JiraNotAuthenticatedException

    def _is_authenticated_in_p1sms(self):
        if self.username not in self.users.get_p1sms_auth_users():
            raise P1smsNotAuthenticatedException

    def dispatch(self):
        if self.command in ['/info', '/инфо']:
            self._show_info()
        if self.command in ['/bug', '/баг']:
            self._is_authenticated_in_jira()
            self._create_bug()
        elif self.command in ['/найти', '/поиск', '/find', '/search']:
            self._is_authenticated_in_jira()
            self._find_issue()
        elif self.command in ['/f', '/ф']:
            self._press_f()
        elif self.command in ['/поднялсерв', '/поднялсервак', '/поднялсервер']:
            self._server_up()
        elif self.command[0:4] == '/дим' and self.command[len(self.command) - 1] == 'н':
            mid = self.command[4:(len(self.command) - 1)]
            if re.sub(r'[^о]', '', mid) == mid:
                self._dimooon(len(mid))
        elif self.command in ['/sms', '/смс']:
            self._is_authenticated_in_p1sms()
            self._find_sms()
        elif self.command in ['/smsbyphone', '/smsphone', '/smsp', '/смст', '/смспотелефону', '/смспотел']:
            self._is_authenticated_in_p1sms()
            self._find_sms_by_phone()
        elif self.command in ['/authjira', '/jira']:
            self._save_jira_credentials()
        elif self.command in ['/jiraproj', '/project', '/proj', '/setproject', '/setproj', '/changeproject', '/changeproj']:
            self._is_authenticated_in_jira()
            self._set_jira_project()


class JiraActions:
    def __init__(self):
        self.bot = Bot(environ.get('TELEGRAM_BOT_ID'), environ.get('TELEGRAM_API_KEY'))
        self.db = DatabaseHelper()

    def task_updated(self, data):
        key = data['issue']['key']
        task_info = self.db.query("SELECT chat_id, url FROM tasks WHERE taskkey = '{taskkey}';".format(
            taskkey=key)).as_dict()
        if len(task_info) == 0:
            return None
        chat_id = task_info[0]['chat_id']
        task_url = task_info[0]['url'] + '/browse/' + key
        changed_fields = data['changelog']['items']
        for changed_field in changed_fields:
            if changed_field['field'] == 'status':
                self.bot.send_message(chat_id,
                                      'Задача {key}: изменён статус с "{fromString}" на "{toString}"\n {url}'.format(
                                          key=key, fromString=changed_field['fromString'],
                                          toString=changed_field['toString'], url=task_url)
                                      )
