from os import environ
from jira import JIRA

from app.utils.database import DatabaseHelper
from app.utils.users import Users


class Jira:
    auth_jira = None
    project = None
    url = None

    def __init__(self):
        self.url = environ.get('JIRA_URL')
        self.auth_jira = JIRA({'server': self.url},
                              timeout=3,
                              max_retries=0,
                              basic_auth=(
                                  environ.get('JIRA_LOGIN'),
                                  environ.get('JIRA_PASSWORD'))
                              )
        self.project = environ.get('JIRA_PROJECT')
        self.users = Users()
        self.db = DatabaseHelper()

    def search_issues_by_description(self, search):
        issues = self.auth_jira.search_issues(
            'project = {project} AND description ~ {search} OR project = {project} AND summary ~ {search}'.format(
                project=self.project,
                search=search
            ))
        return list(self._generate_url_to_issue(issue) for issue in issues)

    def create_issue(self, chat_id, summary, responsible_user, description=''):
        issue = self.auth_jira.create_issue(project=self.project,
                                            summary=summary,
                                            description=description,
                                            issuetype={'name': 'Баг из телеги'})
        if responsible_user in self.users.get_jira_users():
            self.auth_jira.assign_issue(issue.key, self.users.get_jira_user(responsible_user))
        self.db.query('INSERT INTO tasks (taskkey, url, chat_id) VALUES (:taskkey, :url, :chat_id);'.format(
            taskkey=issue.key, url=self.url, chat_id=str(chat_id)))
        return self._generate_url_to_issue(issue)

    def _generate_url_to_issue(self, issue):
        return self.url + '/browse/' + issue.key
