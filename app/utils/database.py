import logging
import psycopg2
import time
from os import environ
from psycopg2 import ProgrammingError
from records import Database
from sqlalchemy.exc import OperationalError, StatementError
from sqlalchemy.pool import QueuePool


class P1smsDecorator:
    def __init__(self, func):
        def get_connection():
            conn = psycopg2.connect(database=environ.get('P1SMS_DB_NAME'),
                                    user=environ.get('P1SMS_DB_USER'),
                                    password=environ.get('P1SMS_DB_PASSWORD'),
                                    host=environ.get('P1SMS_DB_HOST'),
                                    port=environ.get('P1SMS_DB_PORT'))
            conn.autocommit = True
            conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

            DEC2FLOAT = psycopg2.extensions.new_type(
                psycopg2.extensions.DECIMAL.values,
                'DEC2FLOAT',
                lambda value, curs: float(value) if value is not None else None)
            psycopg2.extensions.register_type(DEC2FLOAT)

            return conn

        self.func = func
        self.pool = QueuePool(creator=get_connection, max_overflow=10, pool_size=5)
        self.connection = None
        self.attempts_count = 0
        self.max_attempts_count = int(environ.get('P1SMS_DB_RECONNECT_ATTEMPTS'))

    def __call__(self, query, *args, **kwargs):
        try:
            if self.connection is None:
                self.connection = Database('postgresql+psycopg2://', pool=self.pool)
            self.attempts_count = 0

            return self.func(self.connection, query, *args, **kwargs)
        except OperationalError as error:
            if self.attempts_count < self.max_attempts_count:
                logging.exception('Database error: {error}'.format(error=error))
                time.sleep(60)
                self.attempts_count += 1
                self.connection = None
                return self.__call__(query, *args, **kwargs)
            else:
                logging.critical(error)

    def __del__(self):
        if self.connection:
            self.connection.close()


class BotDecorator:
    def __init__(self, func):
        def get_connection():
            conn = psycopg2.connect(database=environ.get('BOT_DB_NAME'),
                                    user=environ.get('BOT_DB_USER'),
                                    password=environ.get('BOT_DB_PASSWORD'),
                                    host=environ.get('BOT_DB_HOST'),
                                    port=environ.get('BOT_DB_PORT'))
            conn.autocommit = True
            conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

            DEC2FLOAT = psycopg2.extensions.new_type(
                psycopg2.extensions.DECIMAL.values,
                'DEC2FLOAT',
                lambda value, curs: float(value) if value is not None else None)
            psycopg2.extensions.register_type(DEC2FLOAT)

            return conn

        self.func = func
        self.pool = QueuePool(creator=get_connection, max_overflow=10, pool_size=5)
        self.connection = None
        self.attempts_count = 0
        self.max_attempts_count = int(environ.get('P1SMS_DB_RECONNECT_ATTEMPTS'))

    def __call__(self, query, *args, **kwargs):
        try:
            if self.connection is None:
                self.connection = Database('postgresql+psycopg2://', pool=self.pool)
            self.attempts_count = 0

            return self.func(self.connection, query, *args, **kwargs)
        except OperationalError as error:
            if self.attempts_count < self.max_attempts_count:
                logging.exception('Database error: {error}'.format(error=error))
                time.sleep(60)
                self.attempts_count += 1
                self.connection = None
                return self.__call__(query, *args, **kwargs)
            else:
                logging.critical(error)

    def __del__(self):
        if self.connection:
            self.connection.close()


class DatabaseHelper:
    queries = {}

    @staticmethod
    @BotDecorator
    def query(connection, query, *args, **kwargs):
        try:
            result = connection.query(query, *args, **kwargs)
            return result
        except OperationalError:
            raise
        except (ProgrammingError, StatementError) as error:
            logging.exception('Database error: {error}'.format(error=error))

    @staticmethod
    @P1smsDecorator
    def p1sms_query(connection, query, *args, **kwargs):
        try:
            result = connection.query(query, *args, **kwargs)
            return result
        except OperationalError:
            raise
        except (ProgrammingError, StatementError) as error:
            logging.exception('Database error: {error}'.format(error=error))
