class JiraNotAuthenticatedException(Exception):
    """Raised when user not authenticated in jira"""


class P1smsNotAuthenticatedException(Exception):
    """Raised when user call method for only some users"""
